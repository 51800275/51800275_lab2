import java.util.List;
import java.util.Scanner;

public class Program {
	
	private static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		String serverUrl = args[0];
		String user = args[1];
		String password = args[2];
		
		ProductDAO productDao = new ProductDAO(serverUrl, user, password);
		
		if (productDao.getConnection() != null) {
			System.out.println("Connect database succesfully!");
			handleUserChoose(productDao);
		} else {
			System.out.println("Connect database failure!");
			System.out.println("--------End program--------");
		}

	}

	private static void handleUserChoose(ProductDAO productDao) {
		boolean existOption = false;
		do {
			System.out.println("\n--------Options--------");
			System.out.println("1. Read all products.");
			System.out.println("2. Read detail of a product by id.");
			System.out.println("3. Add new product.");
			System.out.println("4. Update a product.");
			System.out.println("5. Delete a product by id.");
			System.out.println("6. Exit.");
			System.out.println("\nYour choice: ");
			String choice = sc.nextLine();
			
			switch (choice) {
			case "1":
				handleReadAllProducts(productDao);
				break;
			case "2":
				handleReadProductDetailById(productDao);
				break;
			case "3":
				handleAddNewProduct(productDao);
				break;
			case "4":
				handleUpdateProduct(productDao);
				break;
			case "5":
				handleDeleteProductById(productDao);
				break;
			case "6":
				existOption = true;
				System.out.println("--------End program--------");
				break;
			default:
				System.out.println("Your select is not exist, please choose again.");
			}
		}while (!existOption);
		
	}

	private static void handleDeleteProductById(ProductDAO productDao) {
		// TODO Auto-generated method stub
		
	}

	private static void handleUpdateProduct(ProductDAO productDao) {
		System.out.println("Enter product's id: ");
		Integer id = sc.nextInt();
		sc.nextLine();
		Product p = productDao.read(id);
		if (p != null) {
			System.out.println("Enter a new name for product: ");
			String name = sc.nextLine();
			System.out.println("Enter a new price for product: ");
			Integer price = sc.nextInt();
			sc.nextLine();
			p.setName(name);
			p.setPrice(price);
			if (productDao.update(p)) {
				System.out.println("Update product successully!");
			} else {
				System.out.println("Update product failure, please try again.");
			}
		} else {
			System.out.println("Product with id=" + id + "doesn't exist.");
		}
	}

	private static void handleAddNewProduct(ProductDAO productDao) {
		System.out.println("Enter product's name ");
		String name = sc.nextLine();
		System.out.println("Enter product's price: ");
		Integer price = sc.nextInt();
		sc.nextLine();
		Product p = new Product();
		p.setName(name);
		p.setPrice(price);
		if (productDao.add(p) != null) {
			System.out.println("Add product successfully!");
		} else {
			System.out.println("Add product failure, please try again.");
		}
		
	}

	private static void handleReadProductDetailById(ProductDAO productDao) {
		System.out.println("Enter product's id: ");
		Integer id = sc.nextInt();
		sc.nextLine();
		Product p = productDao.read(id);
		if (p != null) {
			System.out.println(p.toString());
		} else {
			System.out.println("Product with id=" + id + "doesn't exist.");
		}
	}

	private static void handleReadAllProducts(ProductDAO productDao) {
		List<Product> products = productDao.readAll();
		
		System.out.println("\nProduct list: ");
		for (Product p : products) {
			System.out.println(p.toString());
		}
	}

}
