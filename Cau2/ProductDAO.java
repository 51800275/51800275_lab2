//package Cau02;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProductDAO implements Repository<Product,Integer> {
	
	private String serverUrl;
	private String user;
	private String password;
	
	public ProductDAO(String serverUrl, String user, String password) {
		super();
		this.serverUrl = serverUrl;
		this.user = user;
		this.password = password;
		init();
	}
	
	public Connection getConnection() {
		try {
			Connection conn = DriverManager.getConnection(this.serverUrl + "/ProductManagement", this.user, this.password);
			return conn;
		} catch (SQLException e) {
			return null;
		}
	}
	
	public void init() {
		try {
			Connection conn = DriverManager.getConnection(this.serverUrl, this.user, this.password);
			createIfNotExist(conn);
			createTableIfNotExist(conn);
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void createTableIfNotExist(Connection conn) {
		try {
			String sql1 = "CREATE DATABASE IF NOT EXISTS ProductManagement";
			PreparedStatement ptm1 = conn.prepareStatement(sql1);
			String sql2 = "USE ProductManagement";
			PreparedStatement ptm2 = conn.prepareStatement(sql2);
			
			ptm1.executeUpdate();
			ptm2.execute();
			
			ptm1.close();
			ptm2.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	private void createIfNotExist(Connection conn) {
		try {
			String sql1 = "DROP TABLE IF EXIST Product";
			PreparedStatement ptm1 = conn.prepareStatement(sql1);
			String sql2 = "CREATE TABLE Product(id int NOT NULL AUTO_INCREMENT, name VARCHAR(200), price int, PRIMARY KEY (id))";
			PreparedStatement ptm2 = conn.prepareStatement(sql2);
			
			ptm1.executeUpdate();
			ptm2.execute();
			
			ptm1.close();
			ptm2.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Integer add(Product item) {
		Integer id = null;
		String sql = "INSERT INTO Product(name,price) values(?,?)";
		Connection conn = null;
		PreparedStatement ptm = null;
		ResultSet resultSet = null;
		
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			ptm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ptm.setNString(1, (String) item.getName());
			ptm.setInt(2, (Integer) item.getPrice());
			ptm.executeUpdate();
			resultSet = ptm.getGeneratedKeys();
			if (resultSet.next()) {
				id = resultSet.getInt(1);
			}
			conn.commit();
			return id;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
				if (ptm != null) {
					ptm.close();
				}
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public List<Product> readAll() {
		List<Product> res = new ArrayList<>();
		String sql = "SELECT * FROM product";
		Connection conn = null;
		PreparedStatement ptm = null;
		ResultSet resultSet = null;
		
		try {
			conn = getConnection();
			ptm = conn.prepareStatement(sql);
			resultSet = ptm.executeQuery();
			while (resultSet.next()) {
				Product p = new Product();
				p.setId(resultSet.getInt("id"));
				p.setName(resultSet.getNString("name"));
				p.setPrice(resultSet.getInt("price"));
				res.add(p);
			}
		} catch (SQLException e) {
			// TODO: handle exception
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
				if (ptm != null) {
					ptm.close();
				}
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return res;
	}

	@Override
	public Product read(Integer id) {
		Product res = null;
		String sql = "SELECT * FROM product WHERE id = ?";
		Connection conn = null;
		PreparedStatement ptm = null;
		ResultSet resultSet = null;
		
		try {
			conn = getConnection();
			ptm = conn.prepareStatement(sql);
			ptm.setInt(1, (Integer) id);
			resultSet = ptm.executeQuery();
			if (resultSet.next()) {
				res = new Product();
				res.setId(resultSet.getInt("id"));
				res.setName(resultSet.getNString("name"));
				res.setPrice(resultSet.getInt("price"));
			} else {
				return null;
			}
			return res;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
				if (ptm != null) {
					ptm.close();
				}
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return res;
	}

	@Override
	public boolean update(Product item) {
		String sql = "UPDATE Product SET name = ?, price = ? WHERE id = ?";
		Connection conn = null;
		PreparedStatement ptm = null;
		ResultSet resultSet = null;
		
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			ptm = conn.prepareStatement(sql);
			ptm.setNString(1, (String) item.getName());
			ptm.setInt(2, (Integer) item.getPrice());
			ptm.setInt(3, (Integer) item.getId());
			int rows = ptm.executeUpdate();
			conn.commit();
			
			if (rows == 1) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
				if (ptm != null) {
					ptm.close();
				}
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public boolean delete(Integer id) {
		String sql = "DELETE FROM Product WHERE id = ?";
		Connection conn = null;
		PreparedStatement ptm = null;
		
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			ptm = conn.prepareStatement(sql);
			ptm.setInt(1, (Integer) id);
			int rows = ptm.executeUpdate();
			conn.commit();
			
			if (rows == 1) {
				return true;
			}
		} catch (SQLException e) {
			if (conn != null) {
				try {
					conn.rollback();
				} catch (SQLException e2) {
					e.printStackTrace();
				}
			}
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
				if (ptm != null) {
					ptm.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
}
