import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class Ex03 {

	public static void main(String[] args) {
		String url1 = "jdbc:mysql://localhost:3306/demo";
        String user = "root";
        String password = "";
        try {
        	Connection conn = DriverManager.getConnection(url1, user, password);
			System.out.println("Successful");
			
//			Select from table
			String sql = "SELECT * FROM STUDENT";
			Statement stm = (Statement) conn.createStatement();
			
			ResultSet data = stm.executeQuery(sql);
			ArrayList<Student> lst = new ArrayList<>();
			
			while (data.next()) {
				String name = data.getString(1);
				int age = data.getInt(2);
				Student s = new Student(name,age);
				lst.add(s);
			}
			
			for (Student s : lst) {
				System.out.println(s);
			}
			
			stm.close();
        	conn.close();
			
		} catch (Exception e) {
			System.out.println("An error occurred. Maybe user/password is invalid");
//          ex.printStackTrace();
		}
	}

}
