import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

public class Ex02 {

	public static void main(String[] args) {
		String url1 = "jdbc:mysql://localhost:3306/demo";
        String user = "root";
        String password = "";
        try {
        	Connection conn = DriverManager.getConnection(url1, user, password);
//        	Statement stm = (Statement) conn.createStatement();
			System.out.println("Successful");
			
//			Create table for database
//			String sql = "CREATE TABLE STUDENT" + "(NAME VARCHAR(30) PRIMARY KEY," + " AGE INT)";
//			stm.executeUpdate(sql);
//			System.out.println("Create successful");
			
//			Insert value
			String sqli = "INSERT INTO STUDENT VALUES(?,?)";
			PreparedStatement ptm = (PreparedStatement) conn.prepareStatement(sqli);
			ptm.setString(1, "Hong Duyen");
			ptm.setInt(2, 20);
			
			int rows = ptm.executeUpdate();
			if (rows == 1) {
				System.out.println("Insert successful");
			}
			
//			stm.close();
        	conn.close();
		} catch (Exception e) {
			System.out.println("An error occurred. Maybe user/password is invalid");
//          ex.printStackTrace();
		}
	}

}
